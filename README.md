tikiwiki/diagram
==============

Draw.io component to be used within Tiki®.
This repository mirrors code changes in webapp from main repository (jgraph/drawio).

For more details see https://github.com/jgraph/drawio.
